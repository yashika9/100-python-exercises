import pandas

points_data = pandas.read_csv("points.txt", header=None)
points_data.columns = ["Question", "Points"]

print(points_data["Points"].sum())
