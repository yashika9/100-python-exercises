import random

# abcdefghijklmnopqrstuvwxyz
# ABCDEFGHIJKLMNOPQRSTUVWXYZ
# 0123456789
# !@#$%^&*()

allowed_characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()"

password = ''.join((random.choice(allowed_characters)) for i in range(6))
print("Password is:", password)

# or

password = "".join(random.sample(allowed_characters, 6))
print("Password is:", password)
