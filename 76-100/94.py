with open("94-helper.txt") as file:
    links = file.read().splitlines()

print(links)
new_links = []

for link in links:
    link = link.replace("s", "", 1)
    link = link[:5] + "/" + link[5:]
    new_links.append(link)

print(new_links)

with open("94-helper.txt", "w") as file:
    for link in new_links:
        file.write(link + "\n")
