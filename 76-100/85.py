with open("85-helper.txt") as file:
    content = file.read().splitlines()

content = [i for i in content if i != ""]
content = [i for i in content if i != "Top of Page"]
content = [i for i in content if len(i) != 1]

print(content)

with open("85-helper.txt", "w") as file:
    for i in content:
        file.write(i+"\n")
