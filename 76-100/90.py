import sqlite3
import pandas

conn = sqlite3.connect('90-helper1.db')
cursor = conn.cursor()
cursor.execute("SELECT * FROM countries WHERE area >= 2000000")
rows = cursor.fetchall()
conn.close()

data_frame = pandas.DataFrame(rows, columns =['Rank', 'Country', 'Area', 'Population'])
data_frame.to_csv('90-helper2.csv', index=False)

# or

data_frame = pandas.DataFrame.from_records(rows)
data_frame.columns = ['Rank', 'Country', 'Area', 'Population']
data_frame.to_csv('90-helper3.csv', index=False)
