import re

while True:
    psword = input("Enter new password: ")
    if any(i.isdigit() for i in psword) and any(i.isupper() for i in psword) and len(psword) >= 5:
        print("Password is fine")
        break
    else:
        print("Please check the following:")
        if not any(i.isdigit() for i in psword):
            print("You need at least one number")
        if not any(i.isupper() for i in psword):
            print("You need at least one uppercase letter")
        if not len(psword) >= 5:
            print("You need at least 5 characters")

# or
def digit_present(string):
    return re.search('\d', string)

def upper_present(string):
    return (any(x.isupper() for x in string))

pass_length = 0
while pass_length < 5:
    notes = []
    password = input("Enter your password: ")
    if len(password) < 5:
        notes.append("You need at least 5 characters")
    if not digit_present(password):
        notes.append("You need at least one number")
    if not upper_present(password):
        notes.append("You need at least one uppercase letter")
    if len(notes) == 0:
        print("Password is fine")
        pass_length = len(password)
    else:
        print("Please check the following: ")
        for note in notes:
            print(note)
