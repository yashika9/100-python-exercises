from datetime import date
from datetime import timedelta
import dateutil.relativedelta

today = date.today()
this_year = today.strftime("%Y")

age = int(input("Enter your age: "))

birth_date = today - timedelta(days=age*365)
birth_year = birth_date.strftime("%Y")

print("Your age today is:", age)
print("Year now is", this_year)
print("We think you were born back in", birth_year)

# or

birth_date = today - dateutil.relativedelta.relativedelta(years=age)
birth_year = birth_date.strftime("%Y")
print("We think you were born back in", birth_year)

#or
birth_year = today.year - age
print("We think you were born back in", birth_year)
