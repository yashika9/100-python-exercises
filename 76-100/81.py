filename = "81-users.txt"
with open(filename) as f:
    name_list = f.read().splitlines()
print(name_list)

while True:
    username = input("Enter new username: ")
    username_lower = username.lower()
    if username_lower in (string.lower() for string in name_list):
        print("Username exists")
    else:
        print("Username is fine")
        user_file = open(filename, "a")  # append mode
        user_file.write(username + "\n")
        user_file.close()
        break;

with open(filename) as f:
    name_list = f.read().splitlines()
print(name_list)

while True:
    psword = input("Enter new password: ")
    if any(i.isdigit() for i in psword) and any(i.isupper() for i in psword) and len(psword) >= 5:
        print("Password is fine")
        break
    else:
        print("Please check the following:")
        if not any(i.isdigit() for i in psword):
            print("You need at least one number")
        if not any(i.isupper() for i in psword):
            print("You need at least one uppercase letter")
        if not len(psword) >= 5:
            print("You need at least 5 characters")
