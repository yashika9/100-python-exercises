import re

def digit_present(string):
    return re.search('\d', string)

def upper_present(string):
    return (any(x.isupper() for x in string))

pass_length = 0
while pass_length < 5:
    password = input("Enter your password: ")
    if len(password) < 5:
        print("Password is not fine")
    elif not digit_present(password):
        print("Password is not fine")
    elif not upper_present(password):
        print("Password is not fine")
    else:
        print("Password is:", password)
        pass_length = len(password)

# or

while True:
    psword = input("Enter new password: ")
    if any(i.isdigit() for i in psword) and any(i.isupper() for i in psword) and len(psword) >= 5:
        print("Password is fine")
        break
    else:
        print("Password is not fine")
