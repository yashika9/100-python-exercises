checklist = ["Portugal", "Germany", "Spain"]

with open("87-helper1.txt") as file:
    content = file.read().splitlines()

new_list = sorted(content + checklist)
print(new_list)

with open("87-helper2.txt", "w") as file:
    for i in new_list:
        file.write(i+"\n")

#or

for i in checklist:
    content.append(i)
content = sorted(content)

with open("87-helper3.txt", "w") as file:
    for i in content:
        file.write(i+"\n")
