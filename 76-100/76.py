from datetime import date

today = date.today()

# Textual month, day and year
my_date = today.strftime("%A, %B %d, %Y")
print("Today is", my_date)
