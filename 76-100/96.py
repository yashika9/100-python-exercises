with open("96-helper.txt", "a+") as file:
    text = input("Write a value: ")
    while text != "CLOSE":
        file.write(text + "\n")
        text = input("Write a value: ")

# or

file = open("96-helper.txt", "a+")

while True:
    text = input("Write a value: ")
    if text == "CLOSE":
        file.close()
        break
    else:
        file.write(text + "\n")
