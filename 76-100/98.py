import Tkinter as tk

file = open("98-helper.txt", "a+")
window = tk.Tk()

def collect_data():
    file.write(user_entry.get() + "\n")
    user_entry.delete(0, 'end')

def save_data():
    global file
    file.close()
    file = open("98-helper.txt", "a+")

def close_data():
    global file
    file.close()
    window.destroy()

label = tk.Label(text = "User Input!", width = 50, height = 3, bg = "white", fg = "black")
user_entry = tk.Entry(width = 50, fg="black", bg="white")

add_button = tk.Button(text="Add Line", width=25, height=5, bg="white", fg="black",command = collect_data)
save_button = tk.Button(text="Save Changes", width=25, height=5, bg="white",fg="black",command = save_data)
close_button = tk.Button(text="Save and Close", width=25, height=5, bg="white", fg="black",command = close_data)

label.pack()
user_entry.pack()
add_button.pack()
save_button.pack()
close_button.pack()

window.mainloop()
