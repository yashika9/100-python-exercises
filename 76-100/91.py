import sqlite3
import pandas

new_data = pandas.read_csv("91-helper2.txt")

conn = sqlite3.connect('91-helper1.db')
cursor = conn.cursor()

for index, row in new_data.iterrows():
    print(index)
    print(row["Country"], row["Area"])
    cursor.execute("INSERT INTO countries (country,area) VALUES (?,?);", (row["Country"], row["Area"]))
conn.commit()

cursor.execute("SELECT * FROM countries")
rows = cursor.fetchall()
conn.close()

data_frame = pandas.DataFrame(rows, columns =['Rank', 'Country', 'Area', 'Population'])
data_frame.to_csv('91-helper3.csv', index=False)
