import pandas as pd

country_info = pd.read_csv("88-helper.txt")

country_info['density'] = country_info['population_2013'] / country_info['area_sqkm']

sort_by_density = country_info.sort_values(by='density', ascending=False)

densely_populated = sort_by_density.head(5)

for i in densely_populated['country']:
    print(i)
