from collections import OrderedDict

a = ["1", 1, "1", 2]
print(list(dict.fromkeys(a)))

print(list(OrderedDict.fromkeys(a)))

print(list(set(a)))
