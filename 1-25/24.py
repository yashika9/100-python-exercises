from pprint import pprint

d = dict(a = list(range(1, 11)), b = list(range(11, 21)), c = list(range(21, 31)))

for key in d.keys():
    print(key + " has value " + str(d[key]))


for key in d.keys():
    print(key, "has value", d[key])
