d = {"a": 1, "b": 2, "c": 3}


newDict = dict()
for key,value in d.items():
    if value <= 1:
        newDict[key] = value

print(newDict)


new_d = dict((key, value) for key, value in d.items() if value <= 1)
print(new_d)
