import re

def word_file(f):
    file = open(f, 'r')
    s = file.read()
    file.close()

    l = re.split(r'[, ]', s)
    print(l)
    return len(l)

print(word_file("37-help.txt"))


#or


def count_file(f):
    file = open(f, 'r')
    s = file.read()
    file.close()

    s = s.replace(",", " ")
    l = s.split()
    print(l)
    return len(l)

print(count_file("37-help.txt"))
