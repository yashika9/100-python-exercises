def foo():
    c = 1
    return c
c = foo()
print(c)


#or

def foo():
    global c
    c = 1
    return c
foo()
print(c)
