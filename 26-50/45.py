import string, os

if not os.path.exists("45-helper"):
    os.makedirs("45-helper")
for x in string.ascii_lowercase:
    f = open("45-helper/" + x + ".txt", "w")
    f.write(x + "\n")
    f.close()
