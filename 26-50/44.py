import string

f = open("44-help.txt","w")
l = [string.ascii_lowercase[i:i+3] for i in range(0,26,3)]
print(l)
for x in l:
    f.write(x)
    f.write("\n")
f.close()

#or

letters = string.ascii_lowercase + " "
for l1, l2, l3 in zip(letters[0::3],letters[1::3], letters[2::3]):
    print(l1, l2, l3, "\n")
