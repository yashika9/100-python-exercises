from math import pi,pow

def volume(h, r = 10):
    v1 = (4 * pi * pow(r,3)) / 3
    v2 = (pi * pow(h,2) * (3 * r -h)) / 3
    v = v1 - v2
    return v

print(volume(2))
