import string

f = open("43-help.txt","w")
l = [string.ascii_lowercase[i:i+2] for i in range(0,26,2)]
print(l)
for x in l:
    f.write(x)
    f.write("\n")
f.close()

#or

for l1, l2 in zip(string.ascii_lowercase[0::2],string.ascii_lowercase[1::2]):
    print(l1, l2, "\n")
