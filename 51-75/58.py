import json
from pprint import pprint

f = open("58-helper.json", "r")
data = json.load(f)
pprint(data)
f.close()

data["employees"].append({"firstName":"Albert", "lastName":"Bert"})
pprint(data)

with open("58-helper.json", "w") as file:
    json.dump(data, file, indent=4, sort_keys=True)


#or

with open("58-helper.json", "r+") as file:
    data = json.loads(file.read())
    data["employees"].append(dict(firstName = "Albert", lastName = "Bert"))
    file.seek(0)
    json.dump(data, file, indent=4, sort_keys=True)
