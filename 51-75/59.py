a = [1, 2, 3]

for i in range(len(a)):
    print("Item", a[i], "has index", i)

#or

for index, item in enumerate(a):
    print("Item %s has index %s" % (item, index))
