d = dict(weather = "clima", earth = "terra", rain = "chuva")
word = input("Enter word: ")

#this

l = d.keys()
if word not in l:
    print("That word doesn't exist!")
else:
    print(d[word])

#or

def voc(w):
    try:
        return d[w]
    except KeyError:
        return "That word doesn't exist!"

print(voc(word))
