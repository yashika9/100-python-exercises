import pandas

data1 = pandas.read_csv("http://www.pythonhow.com/data/sampledata.txt")
data2 = pandas.read_csv("http://pythonhow.com/data/sampledata_x_2.txt")

data3 = data1.append(data2)
data3.to_csv("74-helper.txt", index=None)

#or
data4 = pandas.concat([data1, data2])
data4.to_csv("74-helper.txt", index=None)
