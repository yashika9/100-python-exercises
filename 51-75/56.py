import json
d = {"employees":[{"firstName": "John", "lastName": "Doe"},
                {"firstName": "Anna", "lastName": "Smith"},
                {"firstName": "Peter", "lastName": "Jones"}],
"owners":[{"firstName": "Jack", "lastName": "Petter"},
          {"firstName": "Jessy", "lastName": "Petter"}]}

j = json.dumps(d, indent=4)
f = open("56-helper.json", "w")
f.write(j)
f.close()

#or
with open("56-helper.json", "w") as file:
    json.dump(d, file, indent=4)
